<?php
header('Content-Type: text/html;charset=UTF-8');

class getData
{
    /**
     * url по которому ходим в API
     */
    public $url = "http://api.zp.ru/v1/vacancies";

    /**
     *
     */
    public function main()
    {
        $this->getVacanciesByRubrics();
        $this->getTopWords();
    }

    /**
     * Формирует информацию по url
     */
    public function data()
    {
        $data = file_get_contents($this->url);
        if ($data) {
            return json_decode($data, true);
        }
        return null;
    }


    /**
     * Возвращает топ рубрик с количеством повторений
     *
     * формат: ключ => значение
     */
    public function getTopRubrics()
    {
        $rubrics = [];
        $data = $this->data();
        foreach ($data['vacancies'] as $vacancy) {
            foreach ($vacancy['rubrics'] as $rubric) {
                $rubrics[] = $rubric['title'];
            }
        }
        $topRubrics = array_count_values($rubrics);
        arsort($topRubrics);

        return $topRubrics;
    }


    /**
     * Возвращает массив слов из заголовка с количеством вхождений
     *
     * форматЖ ключ => значение
     */
    public function getTopTitles()
    {
        $titles = [];
        $data = $this->data();
        foreach ($data['vacancies'] as $vacancy) {
            $titles[] = $vacancy['header'];
        }
        $titlesInString = implode(' ', $titles);
        $titlesInString= strtolower(str_replace(',',' ', $titlesInString));

        $allTitles = explode(' ', $titlesInString);
        // костылёк для вырезки коротких слов
        $titlesArray = [];
        foreach ($allTitles as $titleWord) {
            if (strlen($titleWord) > 4) {
                $titlesArray[] = $titleWord;
            }
        }

        $titlesArray = array_count_values($titlesArray);
        arsort($titlesArray);

        return $titlesArray;
    }

    /**
     * Вывод топ рубрик с количеством повторений
     */
    public function getVacanciesByRubrics() {
        $topRubrics = $this->getTopRubrics();

        foreach ($topRubrics as $rubric => $num) {
            echo 'Рубрика: ' . $rubric . '. Количество: ' . $num . '<br>';
        }
    }

    /**
     * Вывод топ слов в заголовке с количеством повторений
     */
    public function getTopWords() {
        $topWords = $this->getTopTitles();

        foreach($topWords as $word => $num) {
            echo  ' | ' . $word . ' | ' . $num . ' | <br>';
        }
    }
}
